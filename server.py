import socket
 
s = socket.socket()
print ("Socket successfully created")
 
port = 12345
s.bind(('', port))
print ("socket binded to %s" %(port))
 

s.listen(5)
print ("socket is listening")

c, addr = s.accept()
print ('Got connection from', addr )
 
number = 1
while number < 100:
    data = str(number).encode()
    c.send(data)

    received_data = c.recv(1024)
    number = int(received_data.decode())
    print(number)

c.close()