import socket
 
s = socket.socket()
port = 12345

s.connect(('127.0.0.1', port))
 
while True:
    received_data = s.recv(1024)
    number = int(received_data.decode())

    print(number)

    number = int(number) + 1

    data = str(number).encode()
    s.send(data)

    if int(number) == 100:
        break

s.close()